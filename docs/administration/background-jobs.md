# Background jobs

Application uses [Sidekiq](https://github.com/sidekiq/sidekiq) to run scheduled dependency update jobs and other service jobs. Sidekiq provides a Web UI to view and manage jobs. It is served at `/sidekiq` path of application, like `http://localhost:3000/sidekiq`. This page can be useful to debug issues with jobs.
