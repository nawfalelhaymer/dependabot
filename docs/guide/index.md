<script setup>
import { useData } from 'vitepress'

const { theme } = useData()
</script>

# What is dependabot-gitlab?

It is an app for automatically managing dependency updates.

`dependabot-gitlab` uses [dependabot-core](https://github.com/dependabot/dependabot-core) for dependency update logic and adds additional functionality to integrate these updates with GitLab.

::: warning
dependabot-gitlab is not affiliated with, funded by, or associated with the Dependabot team or GitHub
:::

::: warning
dependabot-gitlab is currently in alpha status. It is already suitable for out-of-the-box dependency updates, but the config options and api might still have breaking changes between minor releases
:::

## Distribution

Application is packaged as a docker image and is available from following registries:

- [Dockerhub](https://hub.docker.com/r/andrcuns/dependabot-gitlab/tags) - `docker.io/andrcuns/dependabot-gitlab:{{ theme.appImageTag }}`
- [GHCR](https://github.com/dependabot-gitlab/images/pkgs/container/dependabot-gitlab) - `ghcr.io/dependabot-gitlab/dependabot-gitlab:{{ theme.appImageTag }}`

::: warning
Because internally application will start updater containers, using `latest` tag is not recommended because updater container can get out of sync with worker container. It is recommended to always use specific version tag instead.
:::

::: tip
It is possible to deploy version of application built from latest commit of `main` branch by using `main-latest` tag in `ghcr.io/dependabot-gitlab/dependabot-gitlab` registry. This tag is updated on every commit to `main` branch and is useful for testing new features or bugfixes before they are released. It is not recommended to use this tag in production as it can contain breaking changes and bugs.
:::
