# frozen_string_literal: true

describe Dependabot::Projects::Remover, :integration do
  let(:project) { create(:project, webhook_id: webhook_id) }

  before do
    allow(Gitlab::Hooks::Remover).to receive(:call)
    allow(Cron::JobRemover).to receive(:call).with(project.name)
  end

  context "with webhook id" do
    let(:webhook_id) { 123 }

    it "removes project and webhook" do
      described_class.call(project)

      expect(Gitlab::Hooks::Remover).to have_received(:call).with(project.name, webhook_id)
      expect(Cron::JobRemover).to have_received(:call).with(project.name)
      expect(Project.where(id: project.id)).to be_empty
    end
  end

  context "without webhook id" do
    let(:webhook_id) { nil }

    it "removes project and skips webhook removal" do
      described_class.call(project)

      expect(Gitlab::Hooks::Remover).not_to have_received(:call)
      expect(Cron::JobRemover).to have_received(:call).with(project.name)
      expect(Project.where(id: project.id)).to be_empty
    end
  end
end
