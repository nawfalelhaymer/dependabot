# frozen_string_literal: true

describe Update::Triggers::DependencyUpdate, :integration do
  subject(:service) { described_class }

  let(:project) { create(:project) }
  let(:update_job) { project.update_jobs.first }
  let(:update_run) { update_job.reload.runs.last }

  let(:args) do
    {
      project_name: project.name,
      package_ecosystem: "bundler",
      directory: "/"
    }
  end

  before do
    allow(Dependabot::UpdateService).to receive(:call)

    RequestStore.clear!
  end

  context "without errors" do
    it "runs dependency updates and saves last enqued time", :aggregate_failures do
      service.call(*args.values)

      expect(Dependabot::UpdateService).to have_received(:call).with(**args)
      expect(update_run.created_at).not_to be_nil
    end
  end

  context "with errors" do
    before do
      allow(Dependabot::UpdateService).to receive(:call).and_raise(StandardError.new("Some error!"))
    end

    it "saves run errors", :aggregate_failures do
      expect { service.call(*args.values) }.to raise_error(StandardError, "Some error!")
      expect(update_run.failures.map(&:message)).to eq(["Some error!"])
    end
  end
end
