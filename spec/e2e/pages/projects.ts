import { Locator, Page, expect } from "@playwright/test";

export class ProjectsPage {
  readonly page: Page;
  readonly projectInput: Locator;
  readonly tokenInput: Locator;
  readonly addButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.projectInput = page.locator("id=project_name");
    this.tokenInput = page.locator("id=access_token");
    this.addButton = page.locator("id=add_project");
  }

  async visit() {
    await this.page.goto("/projects");
  }

  async expectPageToBeVisible() {
    await expect(this.page).toHaveTitle(/Dependabot Gitlab/);
    await expect(this.projectInput).toBeVisible();
    await expect(this.tokenInput).toBeVisible();
  }

  async expectProjectToBeVisible(projectName: string) {
    await expect(this.page.getByText(projectName)).toBeVisible();
  }

  async addProject(projectName: string, token?: string) {
    await this.projectInput.fill(projectName);
    if (token) {
      await this.tokenInput.fill(token);
    }
    await this.addButton.click();
  }
}
