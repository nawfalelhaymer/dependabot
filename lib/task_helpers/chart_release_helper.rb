# frozen_string_literal: true

require_relative "util"

class ChartReleaseHelper
  CHART = "charts/dependabot-gitlab/Chart.yaml"
  README = "README.md"

  def initialize(version)
    @app_version = SemVer.parse(version)
    @chart_repo = "dependabot-gitlab/chart"
  end

  def self.call(version)
    new(version).update
  end

  # Update app version in helm chart
  #
  # @return [void]
  def update
    logger.info("Updating app version to #{app_version}")
    update_version
    create_merge_request
  end

  private

  include Util

  attr_reader :app_version, :chart_repo

  # Update app version
  #
  # @return [void]
  def update_version
    logger.info(" creating branch #{branch_name}")
    gitlab.create_branch(chart_repo, branch_name, "main")
    logger.info(" updating #{CHART}")
    gitlab.create_commit(
      chart_repo,
      branch_name,
      "Update app version to #{app_version}\n\nchangelog: dependency",
      file_actions
    )
  end

  # Create version update mr
  #
  # @return [void]
  def create_merge_request
    logger.info(" creating merge request")
    gitlab.create_merge_request(
      chart_repo,
      "Update app version to #{app_version}",
      source_branch: branch_name,
      target_branch: "main"
    )
  end

  # Branch name
  #
  # @return [String]
  def branch_name
    @branch_name ||= "update-app-version-to-#{app_version}"
  end

  # Updated Chart.yaml
  #
  # @return [String]
  def updated_chart
    chart.gsub(previous_version, new_version)
  end

  # Chart yaml
  #
  # @return [String]
  def chart
    @chart ||= gitlab.file_contents(chart_repo, CHART, "main")
  end

  # Previous app version
  #
  # @return [Integer]
  def previous_version
    @previous_version = YAML.safe_load(chart)["appVersion"]
  end

  # New version
  #
  # @return [Integer]
  def new_version
    @new_version ||= app_version.format(Util::VERSION_FORMAT_PATTERN)
  end

  # Gitlab commit actions
  #
  # @return [Array]
  def file_actions
    [
      {
        action: "update",
        file_path: CHART,
        content: updated_chart
      }
    ]
  end
end
