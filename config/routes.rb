# frozen_string_literal: true

require "sidekiq/web"
require "sidekiq/cron/web"

Rails.application.routes.draw do
  Healthcheck.routes(self)

  mount Sidekiq::Web => "/sidekiq", :constraints => Constraints::Auth.new
  mount Yabeda::Prometheus::Exporter => "/metrics" if AppConfig.metrics
  mount API => "/"

  root AppConfig.anonymous_access ? "projects#index" : "sessions#new"

  get "sign_in", to: "sessions#new"
  post "sign_in", to: "sessions#create"
  delete "logout", to: "sessions#destroy"

  put "/jobs/:id/execute", to: "job#execute", as: "job_execute"

  resources :projects, only: %i[index create update destroy]
end
