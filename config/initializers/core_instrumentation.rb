# frozen_string_literal: true

Dependabot::SimpleInstrumentor.subscribe do |*args|
  name = args.first
  next unless %w[excon.request excon.response].include?(name)

  payload = args.last
  uri = Excon::Utils.request_uri(payload)

  if name == "excon.request"
    ApplicationHelper.log(:debug, "Performing http :#{payload[:method]} request to '#{uri}'", tags: ["core"])
  else
    ApplicationHelper.log(:debug, "Received response from '#{uri}', status: #{payload[:status]}", tags: ["core"])
  end
end
