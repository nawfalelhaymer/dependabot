# frozen_string_literal: true

module Dependabot
  module Projects
    class Remover < ApplicationService
      include ServiceHelpersConcern

      def initialize(project)
        if project.is_a?(Project)
          @project = project
        else
          @project_id = project
        end
      end

      def call
        log(:info, "Removing project '#{project.name}' and update cron jobs")

        remove_webhook
        remove_project
        delete_all_jobs
      end

      private

      attr_reader :project_id

      # Project
      #
      # @return [Project]
      def project
        @project ||= begin
          find_by = project_id.is_a?(Integer) ? { id: project_id } : { name: project_id }
          Project.find_by(**find_by)
        end
      end

      # Remove project webhook
      #
      # @return [void]
      def remove_webhook
        return unless project.webhook_id

        init_gitlab(project)
        Gitlab::Hooks::Remover.call(project.name, project.webhook_id)
      rescue Gitlab::Error::Error => e
        log_error(e, message_prefix: "Failed to remove webhook for project #{project.name}")
      end

      # Delete project
      #
      # @return [void]
      def remove_project
        project.destroy
      rescue Mongoid::Errors::DocumentNotFound
        log(:error, "Project #{project.name} doesn't exist!")
      end

      # Delete dependency update jobs
      #
      # @return [void]
      def delete_all_jobs
        Cron::JobRemover.call(project.name)
      end
    end
  end
end
