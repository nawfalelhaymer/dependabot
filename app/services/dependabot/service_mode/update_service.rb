# frozen_string_literal: true

module Dependabot
  module ServiceMode
    module UpdateService
      include ServiceHelpersConcern

      # Initialize gitlab client and sync config and run update
      #
      # @return [void]
      def call
        init_gitlab(project)
        sync_config

        super
      end

      # Fetch up to date config if app is not integrated
      #
      # @return [void]
      def sync_config
        return if AppConfig.integrated?

        project.configuration = fetch_config
      end

      # Persisted project
      #
      # @return [Project]
      def project
        @project ||= Project.find_by(name: project_name)
      end

      # Close obsolete vulnerability alerts
      #
      # @param [Dependabot::Dependencies::UpdatedDependency] dependency
      # @return [void]
      def close_obsolete_vulnerability_issues(dependency)
        vulnerability_issues = project.open_vulnerability_issues(
          package_ecosystem: package_ecosystem,
          directory: directory,
          package: dependency.name
        )
        obsolete_issues = vulnerability_issues.reject { |issue| issue.vulnerability.vulnerable?(dependency.version) }
        return if obsolete_issues.empty?

        log(:info, "  closing obsolete vulnerability issues")
        obsolete_issues.each do |issue|
          log(:debug, "   closing obsolete issue !#{issue.iid} because dependency version is not vulnerable")
          Gitlab::Vulnerabilities::IssueCloser.call(issue)
        rescue Gitlab::Error::ResponseError => e
          log_error(e, message_prefix: "   failed to close vulnerability issue")
        end
      end
    end
  end
end
