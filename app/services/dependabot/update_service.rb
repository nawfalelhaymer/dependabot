# frozen_string_literal: true

module Dependabot
  class TooManyRequestsError < StandardError
    def message
      "GitHub API rate limit exceeded! See: https://docs.github.com/en/rest/overview/resources-in-the-rest-api#rate-limiting"
    end
  end

  class ExternalCodeExecutionError < StandardError
    def initialize(package_ecosystem, directory)
      super(<<~ERR)
        Unexpected external code execution detected.
        Option 'insecure-external-code-execution' must be set to 'allow' for entry:
          package_ecosystem - '#{package_ecosystem}'
          directory - '#{directory}'
      ERR
    end
  end

  # Main entrypoint class for updating dependencies and creating merge requests
  #
  class UpdateService < UpdateBase # rubocop:disable Metrics/ClassLength
    include ServiceModeConcern

    def initialize(project_name:, package_ecosystem:, directory:, dependency_name: nil)
      super(project_name)

      @package_ecosystem = package_ecosystem
      @directory = directory
      @dependency_name = dependency_name
    end

    # Create or update mr's for dependencies
    #
    # @return [void]
    def call
      fetch_vulnerabilities
      group_dependencies

      update
    rescue Octokit::TooManyRequests
      raise TooManyRequestsError
    rescue Dependabot::UnexpectedExternalCode
      raise ExternalCodeExecutionError.new(package_ecosystem, directory)
    ensure
      FileUtils.rm_r(repo_contents_path, force: true, secure: true) if repo_contents_path
    end

    private

    attr_reader :package_ecosystem, :directory, :dependency_name

    # Close obsolete vulnerability alerts
    #
    # @param [Dependabot::Dependencies::UpdatedDependency] dependency
    # @return [void]
    def close_obsolete_vulnerability_issues(_updated_dependency); end

    # Project
    #
    # @return [Project]
    def project
      @project ||= fetch_config.then do |config|
        fork_id = config_entry(config)[:fork] ? gitlab.project(project_name).to_h.dig("forked_from_project", "id") : nil
        Project.new(name: project_name, configuration: config, forked_from_id: fork_id)
      end
    end

    # Open mr limits
    #
    # @return [Number]
    def limits
      @limits ||= {
        mr: config_entry[:open_merge_requests_limit],
        security_mr: config_entry[:open_security_merge_requests_limit]
      }
    end

    # Update type, security fix or dependency bump
    #
    # @param [Dependabot::Dependencies::UpdatedDependency] dependency
    # @return [Symbol]
    def update_type(dependency)
      dependency.vulnerable ? :security_mr : :mr
    end

    # Project configuration fetched from gitlab
    #
    # @return [Config]
    def fetch_config
      Config::Fetcher.call(project_name)
    end

    # Vulnerability alerts enabled
    #
    # @return [Boolean]
    def vulnerability_alerts?
      config_entry.dig(:vulnerability_alerts, :enabled)
    end

    # Update project dependencies
    #
    # @return [void]
    def update
      if config_entry[:groups] && dependency_groups&.none? { |group| !group.dependencies.empty? }
        log(:warn, "None of the dependencies matched the rules defined in the groups configurations!")
      end

      # TODO: Perform updates for grouped and ungrouped dependencies once supported
      dependencies.each_with_object({ mr: Set.new, security_mr: Set.new }) do |dep, count|
        # updating dependency is using Dir.chdir so thread needs to be locked
        updated_dep = Semaphore.synchronize { updated_dependency(dep) }
        next if update_dependency(updated_dep, count) # go to next dep if mr was created or updated

        create_vulnerability_issues(updated_dep)
        close_obsolete_vulnerability_issues(updated_dep)
      end
    end

    # Create dependency update merge request
    #
    # @param [Dependabot::Dependencies::UpdatedDependency] dependency
    # @param [Hash] mrs
    # @return [void]
    def update_dependency(dependency, mrs)
      type = update_type(dependency)

      return unless dependency.updates?
      return unless create_mr?(mrs, type)

      iid = MergeRequest::CreateService.call(
        project: project,
        fetcher: fetcher,
        config_entry: config_entry,
        updated_dependency: dependency,
        credentials: credentials
      )&.iid

      mrs[type] << iid if iid
    rescue StandardError => e
      capture_error(e)
      nil
    end

    # Handle vulnerability issues
    #
    # @param [Dependabot::Dependencies::UpdatedDependency] updated_dependency
    # @return [void]
    def create_vulnerability_issues(updated_dependency)
      return if DependabotConfig.dry_run?
      return unless updated_dependency.vulnerable?

      unless vulnerability_alerts?
        log(:warn, "  dependency has vulnerability but vulnerability issue creation is disabled!")
        return
      end

      log(:info, "  creating vulnerability issues for dependency")
      updated_dependency.actual_vulnerabilities.each do |vulnerability|
        Gitlab::Vulnerabilities::IssueCreator.call(
          project: project,
          vulnerability: vulnerability,
          dependency_file: updated_dependency.dependency_files.reject(&:support_file).first,
          assignees: config_entry.dig(:vulnerability_alerts, :assignees),
          confidential: config_entry.dig(:vulnerability_alerts, :confidential)
        )
      rescue Gitlab::Error::ResponseError => e
        log_error(e, message_prefix: "   failed to create issues for vulnerabilities: #{vulnerability.identifiers}")
      end
    end

    # Check if mr should be created based on limits settings
    #
    # @param [Hash] mrs
    # @param [Symbol] type
    # @return [Boolean]
    def create_mr?(mrs, type)
      limit = limits[type]

      return true if limit.negative?
      return true if !limit.zero? && (mrs[type].length < limit)

      dep_type = type == :mr ? "dependency" : "vulnerable dependency"
      reason = if limits[type].zero?
                 "due to disabled mr creation setting!"
               else
                 "due to max open mr limit reached, limit: '#{limit}'!"
               end

      log(:info, "  skipping update of #{dep_type} #{reason}")
      false
    end

    # Fetch package ecosystem vulnerability info
    #
    # @return [void]
    def fetch_vulnerabilities
      return unless AppConfig.standalone? && vulnerability_alerts?

      Github::Vulnerabilities::LocalStore.call(package_ecosystem)
    end
  end
end
