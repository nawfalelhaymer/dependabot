# frozen_string_literal: true

module Gitlab
  module MergeRequest
    module ServiceMode
      module Creator
        # Create merge request
        #
        # @return [Gitlab::ObjectifiedHash]
        def call
          super
          return unless mr

          persist_mr
          mr
        end

        private

        # Close superseded merge request
        #
        # @param [MergeRequest] superseded_mr
        # @return [void]
        def close_superseded_mr(superseded_mr)
          closed = super
          superseded_mr.close if closed
        end

        # Persist merge request
        #
        # @return [void]
        def persist_mr
          ::MergeRequest.create!(
            project: project,
            id: mr.id,
            iid: mr.iid,
            web_url: mr.web_url,
            package_ecosystem: config_entry[:package_ecosystem],
            directory: config_entry[:directory],
            state: "opened",
            auto_merge: updated_dependency.auto_mergeable?,
            squash: squash?,
            update_from: updated_dependency.previous_versions,
            update_to: updated_dependency.current_versions,
            main_dependency: updated_dependency.name,
            branch: gitlab_creator.branch_name,
            target_branch: fetcher.source.branch,
            commit_message: gitlab_creator.commit_message,
            target_project_id: target_project_id
          )
        end

        # List of open superseded merge requests
        #
        # @return [Mongoid::Criteria]
        def superseded_mrs
          project.superseded_mrs(
            update_from: updated_dependency.previous_versions,
            package_ecosystem: config_entry[:package_ecosystem],
            directory: config_entry[:directory],
            mr_iid: mr.iid
          )
        end

        # MR message footer with available commands
        #
        # @return [String]
        def message_footer
          return unless AppConfig.integrated?

          <<~MSG
            ---

            <details>
            <summary>Dependabot commands</summary>
            <br />
            You can trigger Dependabot actions by commenting on this MR

            - `#{AppConfig.commands_prefix} recreate` will recreate this MR rewriting all the manual changes and resolving conflicts

            </details>
          MSG
        end
      end
    end
  end
end
