# frozen_string_literal: true

module ServiceHelpersConcern
  # Initialize gitlab client with project specific token
  #
  # @param [Project] project
  # @return [void]
  def init_gitlab(project)
    token = project.gitlab_access_token
    return unless token

    log(:debug, "Initializing gitlab client with project specific token")
    Gitlab::ClientWithRetry.client_access_token = token
  end

  # Contaienr runner class
  #
  # @return [Update::Container::Base]
  def container_runner_class
    case UpdaterConfig.deploy_mode
    when UpdaterConfig::K8S_DEPLOYMENT
      Container::Kubernetes::Runner
    when UpdaterConfig::COMPOSE_DEPLOYMENT
      Container::Compose::Runner
    else
      raise("Unsupported deploy mode: #{UpdaterConfig.deploy_mode}")
    end
  end
end
