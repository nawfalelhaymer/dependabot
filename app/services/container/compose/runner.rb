# frozen_string_literal: true

require "yaml"
require "open3"

module Container
  module Compose
    class Runner < Base
      def call
        super

        log(:debug, "Starting updater container using image '#{updater_image}'")
        pull_updater_image
        run_update
      end

      private

      attr_reader :compose_project_name

      # Raw compose yml
      #
      # @return [Hash]
      def compose_yml
        @compose_yml ||= YAML.load_file(UpdaterConfig.updater_template_path, aliases: true)
      end

      # Updater container configuration
      #
      # @return [Hash]
      def updater_conf
        @updater_conf ||= {
          "version" => compose_yml["version"],
          "services" => {
            "updater" => {
              "image" => updater_image,
              **compose_yml["x-updater"]
            }
          }
        }
      end

      # Updater docker image
      #
      # @return [String]
      def updater_image
        @updater_image ||= format(UpdaterConfig.updater_image_pattern, package_ecosystem: package_ecosystem)
      end

      # Compose run command
      #
      # @return [Array]
      def compose_run_cmd
        cmd_base = [
          "docker", "compose", "-f", "-", "run", "--no-TTY", "--quiet-pull",
          "--name", "updater-#{package_ecosystem}-#{unique_name_postfix}"
        ]
        cmd_base << "--rm" if UpdaterConfig.delete_updater_container?

        [*cmd_base, "updater", "bundle", "exec", "rake", rake_task]
      end

      # Force pull updater image
      #
      # @return [void]
      def pull_updater_image
        return unless UpdaterConfig.compose_updater_always_pull?

        log(:debug, "Pulling updater image")
        run_shell_cmd(["docker", "pull", "--quiet", updater_image], print_output: false)
      end

      # Run dependency update
      #
      # @return [void]
      def run_update
        run_shell_cmd(compose_run_cmd, stdin_data: updater_conf.to_yaml)
      end

      # :reek:ControlParameter

      # Run shell command
      #
      # @param [Array] cmd <description>
      # @param [String] stdin_data <description>
      # @param [Boolean] print_output <description>
      # @return [void]
      def run_shell_cmd(cmd, stdin_data: nil, print_output: true)
        Open3.popen3(*cmd) do |stdin, out, err, wait_thr|
          if stdin_data
            stdin.write(stdin_data)
            stdin.close
          end

          out.each { |line| puts line.strip } if print_output # rubocop:disable Rails/Output
          next if wait_thr.value.success?

          handle_command_failure(err.read, wait_thr.value.exitstatus)
        end
      end

      # Handle failure of update run
      #
      # @param [String] stderr
      # @return [void]
      def handle_command_failure(stderr, exit_code)
        msg = "Command exited with code: #{exit_code}!"

        log(:error, msg)
        log(:error, "Command '#{compose_run_cmd.join(' ')}' stderr output:\n#{stderr}".strip) if stderr.present?
        raise(Failure, msg)
      end
    end
  end
end
