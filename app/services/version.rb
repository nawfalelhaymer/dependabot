# frozen_string_literal: true

class Version
  VERSION = "2.0.0-alpha.3"
end
