# frozen_string_literal: true

module AuthenticationSetup
  def self.included(base)
    base.class_eval do
      before do
        next if AppConfig.anonymous_access

        error!("Missing basic authorization", 401) unless headers["Authorization"]

        username, password = headers["Authorization"]
                             .split(" ").last
                             .then { |encoded| Base64.decode64(encoded).split(":") }

        user = User.find_by(username: username)
        error!("Unauthorized", 401) unless user.authenticate(password)
      rescue Mongoid::Errors::DocumentNotFound
        error!("Unauthorized", 401)
      end
    end
  end
end
