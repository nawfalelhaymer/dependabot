# frozen_string_literal: true

module V2
  module Hooks
    module Helpers
      # Authenticate request against gitlab token
      #
      # @return [void]
      def authenticate!
        gitlab_auth_token = CredentialsConfig.gitlab_auth_token
        return unless gitlab_auth_token

        gitlab_token = headers["X-Gitlab-Token"] || ""
        return if ActiveSupport::SecurityUtils.secure_compare(gitlab_token, gitlab_auth_token)

        error!("Invalid gitlab authentication token", 401)
      end

      # Full project path
      #
      # @return [String]
      def project_name
        params.dig(:project, :path_with_namespace)
      end

      def project
        ::Project.find_by(name: project_name)
      end
    end
  end
end
